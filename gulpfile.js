'use strict';

let path = require('path');
let gulp = require('gulp');
let less = require('gulp-less');
let clean = require('gulp-clean');
let cleanCSS = require('gulp-clean-css');
let minify = require('gulp-minify');

//CONFIG PATHS
let config = {
    dist : 'dist',
    npm  : 'node_modules',
    less : 'src/less',
    js   : 'src/js'
};

gulp.task('less', function () {
    return gulp.src(config.less+'/style.less')
        .pipe(less({
            paths: [config.less + '/']
        }))
        .pipe(cleanCSS({compatibility: 'ie11'}))
        .pipe(gulp.dest(config.dist + '/css/'))
});

gulp.task('js-min', function(){
    return gulp.src( [config.js+'/**' ])
        .pipe(minify({
            ext: {
                min: '.min.js'
            },
            noSource: true
        }))
        .pipe(gulp.dest(config.dist+'/js'))
});

gulp.task('clean', function(){
    return gulp.src( [config.dist+'/css/*.css', config.dist+'/js/*.js'] , {read: false})
        .pipe(clean());
});

gulp.task('watch', function () {
    gulp.watch(config.assets+'/less/*.less', function(event) {
        gulp.run('build-clean');
    });

    gulp.watch(config.assets+'/js/*.js', function(event) {
        gulp.run('build-clean');
    });

    gulp.watch(config.assets+'/js/**/*.js', function(event) {
        gulp.run('build-clean');
    });
});

gulp.task('build', ['clean', 'less', 'js-min'], function() {});

gulp.task('watch', function () {
    gulp.watch(config.less+'/*.less', function(event) {
        gulp.run('build');
    });

    gulp.watch(config.js+'/*.js', function(event) {
        gulp.run('build');
    });
});

gulp.task('default', function() {
    console.log( "\nPage - Gulp Command List \n" );
    console.log( "----------------------------\n" );
    console.log( "gulp watch" );
    console.log( "gulp less" );
    console.log( "gulp js-min" );
    console.log( "gulp clean" );
    console.log( "gulp build \n" );
    console.log( "----------------------------\n" );
});