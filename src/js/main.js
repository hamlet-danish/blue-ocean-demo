'use strict';

(function($){

    $(document).ready(function(){
        $('select#form-state').select2({
            minimumResultsForSearch: Infinity
        }).on('select2:select', function(e){
            if(e.params.data.id === 'candidate'){
                $('a#candidate-tab').click();
            }else{
                $('a#business-tab').click();
            }
        });

        $('div.form-box a.nav-link').on('click', function(){
            var select = $('select#form-state');
            if($(this).attr('id') === 'candidate-tab'){
                select.val('candidate');
            }else{
                select.val('business');
            }
            select.trigger('change');
        });

        $('form[name="businessContact"]').validate({
            rules: {
                "name": "required",
                "email": {
                    required: true,
                    email: true
                },
                "phone": {
                    required: false,
                    digits: true
                }
            },
            submitHandler: submitHandler
        });

        $('form[name="candidateContact"]').validate({
            rules: {
                "name": "required",
                "email": {
                    required: true,
                    email: true
                },
                "phone": {
                    required: false,
                    digits: true
                }
            },
            submitHandler: submitHandler
        });

        $('a.cta').on('click', function(e){
            e.stopPropagation();
            e.preventDefault();

            var section = $('section.contact-section');
            if(section.length){
                $('html, body').animate({
                    scrollTop: section.offset().top
                }, 1500);
            }
        });

        $('a.slide-arrow').on('click', function(e){
            e.stopPropagation();
            e.preventDefault();

            var direction = $(this).hasClass('left') ? 'left' : 'right';
            var active = $('div.slides-wrapper div.slide.active');
            var next = getNextSlide(active, direction);

            if(next && next.length){
                active.fadeOut('fast', function(){
                    $(this).removeClass('active');

                    $(next).fadeIn('fast', function(){
                        $(this).addClass('active');

                        var left = getNextSlide(next, 'left');
                        var right = getNextSlide(next, 'right');

                        if(!left || !left.length){
                            $('a.slide-arrow.left').attr('disabled', 'disabled');
                        }else{
                            $('a.slide-arrow.left').removeAttr('disabled');
                        }

                        if(!right || !right.length){
                            $('a.slide-arrow.right').attr('disabled', 'disabled');
                        }else{
                            $('a.slide-arrow.right').removeAttr('disabled');
                        }
                    })
                })
            }
        });

        function getNextSlide(currentSlide, direction){
            var next = false;

            if(direction === 'left'){
                next = $(currentSlide).prev();
            }else{
                next = $(currentSlide).next();
            }

            return next;
        }
    });

    function submitHandler(form){
        var data = $(form).serializeArray();
        var clean = [];
        var type = 'Business';
        var body = '';

        if($(form).hasClass('sent')){
            return;
        }

        if($(form).attr('name') === 'candidateContact'){
            type = 'Candidate'
        }

        $(data).each(function(){
            body += this.name.replace(/\b\w/g, function(l){ return l.toUpperCase() }) + ': ' + this.value + "<br/>";
        });

        Email.send("James.clark@blueoceansourcing.com.au",
            "James.clark@blueoceansourcing.com.au",
            type + " contact form submission",
            body,
            {
                token: "75ac62a3-c46d-49f2-9b08-62f968026854"
            }
        );

        $(form).find('input, textarea').each(function(){
            $(this).val('').attr('disabled', 'disabled');
        });

        $(form).find('button').attr('disabled', 'disabled').text('Sent');
        $(form).addClass('sent');
    }
})(jQuery);